/*
 * grunt-file-overide
 * https://github.com/amido/grunt-file-overide
 *
 * Copyright (c) 2015 Christopher Slater
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    jshint: {
      all: [
        'Gruntfile.js',
        'tasks/*.js',
        '<%= nodeunit.tests %>'
      ],
      options: {
        jshintrc: '.jshintrc'
      }
    },

    // Before generating any new files, remove any previously-created files.
    clean: ["tmp"],

    // Configuration to be run (and then tested).
    fileoveride: {
        patterns: ['de', 're.de', 'es'],
        src: 'test/nested',
        root: 'tmp',
        task: 'build:post'
    },

    copy: {
      files: {
        expand: true,
        src: ['test/nested/**'],
        dest: 'tmp'
      }
    },

    browserify: {
      dist: {
        files: {
            'test/build/application.js': ['tmp/test/nested/folder/index.js']
        },
      }
    },

    // Unit tests.
    nodeunit: {
      tests: ['test/*_test.js']
    }

  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-browserify');


  // Whenever the "test" task is run, first clean the "tmp" dir, then run this
  // plugin's task(s), then test the result.
  grunt.registerTask('test', ['nodeunit']);
  grunt.registerTask('build', ['fileoveride', 'browserify']);
  //grunt.registerTask('build:post', ['browserify', 'clean']);
  //grunt.registerTask('build', ['copy', 'fileoveride', 'concat']);

  // By default, lint and run all tests.
  //grunt.registerTask('default', ['jshint', 'build', 'test', 'clean']);

  grunt.registerTask('default', ['jshint', 'build', 'test']);

};
