'use strict';

var grunt = require('grunt');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit

  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

exports.fileoveride = {
  applicationExists: function (test) {
    var file = grunt.file.exists('test/build/application.js');
    test.equal(file, true);
    test.done();
  },


  applicationDeExists: function (test) {
    var file = grunt.file.exists('test/build/application.de.js');
    test.equal(file, true);
    test.done();
  },

  applicationReDeExists: function (test) {
    var file = grunt.file.exists('test/build/application.re.de.js');
    test.equal(file, true);
    test.done();
  },
};
