/*
 * grunt-file-overide
 * https://github.com/amido/grunt-file-overide
 *
 * Copyright (c) 2015 Christopher Slater
 * Licensed under the MIT license.
 */

'use strict';


var taskName = 'fileoveride';
var fs = require('fs-extra');
var path = require('path');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerTask(taskName, 'Browserify centric plugin that allows multiple builds, rename files with known extensions in order to provide internationalised single page apps', function() {
    var config = grunt.config.get(taskName);
    var patterns = config.patterns;
    var root = config.root;

    if (!patterns instanceof Array || !patterns.length > 0) {
      throw new Error(taskName + ': locales must be an Array and contain at least one value');
    }

    patterns.forEach(function (pattern) {
      var pathFrom = path.join(process.cwd(), config.src);
      var pathTo = path.join(process.cwd(), root + '/' + pattern);

      fs.copySync(pathFrom, pathTo);

      //var results = grunt.file.expand(pathTo + '/**/*.' + pattern + '.js');
      grunt.file.recurse(pathTo, function (abspath, rootdir, subdir, filename) {

        var parse = path.parse(abspath);
        var pathname = path.dirname(abspath);
        var ext = path.extname(filename);
        var basename = parse.name.split('.')[0];
        var pat = parse.name.split('.');


        console.log(match, basename + ext, pattern);


        // if (arr.join('.') === pattern) {
        //   console.log(abspath, arr.join('.'));
        //   grunt.file.copy(abspath, supposedFile);
        // }

        //console.log(pattern, arr.join('.'), path + '.' + ext);
        //console.log(pattern + '.', abspath, filename.replace(pattern, ''));
      });
    });

  });

};
